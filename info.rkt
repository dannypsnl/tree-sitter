#lang info
(define collection "racket-tree-sitter")
(define deps '("base"))
(define build-deps '("scribble-lib" "racket-doc" "rackunit-lib"))
(define scribblings '(("scribblings/racket-tree-sitter.scrbl" ())))
(define pkg-desc "Description Here")
(define version "0.0")
(define pkg-authors '(linzizhuan))
(define license '(Apache-2.0 OR MIT))
